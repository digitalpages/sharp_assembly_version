﻿using System;
using System.IO;
using System.Threading.Tasks;
using PCLStorage;

namespace ApplyVertsionToAssemblies
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var task = ChangeVersions(args);

			Task.WaitAll(new Task[] { task });

			Console.WriteLine("concluido");
		}

		private static async Task ChangeVersions(string[] args)
		{
			var version = args[1];
			var folder = args[0];

			var assemblies = Directory.GetFiles(folder, "AssemblyInfo.cs", SearchOption.AllDirectories);

			await ChangeAssemblyVersion(assemblies, version);

			assemblies = Directory.GetFiles(folder, "*.nuspec", SearchOption.AllDirectories);

			await ChangeNuspecVersion(assemblies, version);
		}

		private static async Task ChangeNuspecVersion(string[] assemblies, string version)
		{
			foreach (var path in assemblies)
			{
				string result = null;
				var resGetFile = await FileSystem.Current.GetFileFromPathAsync(path).ConfigureAwait(false);

				using (var stream = await resGetFile.OpenAsync(PCLStorage.FileAccess.Read).ConfigureAwait(false))
				using (var streamreader = new StreamReader(stream))
				{
					result = await streamreader.ReadToEndAsync().ConfigureAwait(false);
				}

				if (string.IsNullOrEmpty(result)) continue;

				result = ChangeVersion(result, version, "<version>", "</version>");

				await resGetFile.WriteAllTextAsync(result);
			}
		}

		private static async Task ChangeAssemblyVersion(string[] assemblies, string v)
		{
			foreach (var path in assemblies)
			{
				string result = null;
				var resGetFile = await FileSystem.Current.GetFileFromPathAsync(path).ConfigureAwait(false);

				using (var stream = await resGetFile.OpenAsync(PCLStorage.FileAccess.Read).ConfigureAwait(false))
				using (var streamreader = new StreamReader(stream))
				{
					result = await streamreader.ReadToEndAsync().ConfigureAwait(false);
				}

				if (string.IsNullOrEmpty(result)) continue;

				result = ChangeVersion(result, v, "AssemblyVersion(\"", "\")]");
				result = ChangeVersion(result, v, "AssemblyFileVersion(\"", "\")]");

				await resGetFile.WriteAllTextAsync(result);
			}
		}

		private static string ChangeVersion(string content, string value, string initKey, string endKey)
		{
			var index = content.IndexOf(initKey, StringComparison.InvariantCulture);

			while (index != -1)
			{
				var last = content.IndexOf(endKey, index, StringComparison.InvariantCulture);
				var info = content.Substring(index, last - index);
				var final = $"{initKey}{value}";

				content = content.Replace(info, final);
				index = content.IndexOf(initKey, Math.Min(index + initKey.Length, content.Length), StringComparison.InvariantCulture);
			}

			return content;
		}
	}
}
